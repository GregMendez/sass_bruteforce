/*
    Author : Mendez Grégory
    Description : bruteforce TP
    Date : 08/03/2022
*/
const HASH = "cnFrbg==";
const NB_LETTER = 4
const CHARS_ARRAY = ["a","b","c","d","e","f","g","h","i","j,","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"," "];

let hashedWord = ""
let generateWord = ""
let isDiscovered = false

//fonction de hashage
const calculateHash = str => {
    let hash = str
        .split("")
        .map((c, i) => str.charCodeAt(i))
        .map(c => c + 2)
        .map(c => String.fromCharCode(c))
        .join("");
    
    return Buffer.from(hash).toString("base64");
}

console.time('time')
while (!isDiscovered) {

    generateWord = ""

    // Tant que le nombre de caractères du mot n'est pas 4
    while (generateWord.length < NB_LETTER) {

        // Ajoute un caractère "aléatoire" du tableau de caractères au mot
        generateWord += CHARS_ARRAY[Math.floor(Math.random() * CHARS_ARRAY.length)];
    }    

    // Hash le mot généré
    hashedWord = calculateHash(generateWord)

    // Vérifie si le hash du mot généré est le même que celui de la consigne
    if(hashedWord === HASH){
        console.log("ok")
        console.log("word : " + generateWord)
        console.log("hashed word : " + hashedWord)
        isDiscovered = true;
    }
}
console.timeEnd('time')





    
 


